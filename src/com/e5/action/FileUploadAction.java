package com.e5.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class FileUploadAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String path;
	private String savePath;//upload
	private File userImage;
	private String userImageContentType;
	private String userImageFileName;


	public FileUploadAction() {
		System.out.println("FileUploadAction constructed...");
	}

	private void close(FileInputStream fis, FileOutputStream fos) {
		if (fis!=null) {
			try {
				fis.close();
				fis=null;
			} catch (Exception e) {
				System.err.println("FileInputStream 关闭失败");
				e.printStackTrace();
			}
		}
		if(fos!=null){
			try {
				fos.close();
				fos=null;
			} catch (Exception e) {
				System.err.println("FileOutputStream 关闭失败");
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("resource")
	@Override
	public String execute() throws Exception {
		//参考资料：http://blog.csdn.net/logan676/article/details/16369399
		
		System.out.println("userImage: "+this.userImage);
		System.out.println("userImageContentType: "+this.getUserImageContentType());
		System.out.println("userImageFileName:" + this.getUserImageFileName());
		// 1.把上传的文件放到指定的路径下
		path = ServletActionContext.getServletContext().getRealPath(this.getSavePath());
		System.out.println("path: " +path);
		// 2.写到指定的路径中
		String img = path + File.separator + this.getUserImageFileName();
		System.out.println("img: "+ img);
		// 3.建立输入、输出流
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(this.getUserImage());
			fos=new FileOutputStream(img);
			byte[] buff = new byte[1024];
			int len=0;
			while ((len>fis.read(buff))) {
				fos.write(buff, 0, len);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("文件上传失败");
			
		}finally{
			close(fis, fos);
		}
		
		return "upload_success";
	}


	public String getPath() {
		return path;
	}


	public String getSavePath() {
		return savePath;
	}


	public File getUserImage() {
		return userImage;
	}

	
	public String getUserImageContentType() {
		return userImageContentType;
	}


	public String getUserImageFileName() {
		return userImageFileName;
	}

	public void setPath(String path) {
		this.path = path;
	}


	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}


	public void setUserImage(File userImage) {
		this.userImage = userImage;
	}

	public void setUserImageContentType(String userImageContentType) {
		this.userImageContentType = userImageContentType;
	}

	public void setUserImageFileName(String userImageFileName) {
		this.userImageFileName = userImageFileName;
	}

	@Override
	public void validate() {
		
		super.validate();
	}

}
