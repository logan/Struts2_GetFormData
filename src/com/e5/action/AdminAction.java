package com.e5.action;

import com.opensymphony.xwork2.ActionSupport;

public class AdminAction extends ActionSupport{
	public String add() {
		System.out.println("AdminAction.add...");
		return SUCCESS;
	}
	public String delete() {
		System.out.println("AdminAction.delete...");
		return SUCCESS;
	}
	public String modify() {
		System.out.println("AdminAction.modify...");
		return SUCCESS;
	}
}
