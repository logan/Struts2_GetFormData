package com.e5.action;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.e5.dao.UserDao;
import com.e5.model.User;
import com.opensymphony.xwork2.ActionSupport;

public class RegisterAction extends ActionSupport  {
	private String account;
	private String confirmPassword;
	private String email;
	private String password;
	
	public String execute() {
		User user=new User();
		user.setAccount(account);
		user.setPassword(password);
		user.setEmail(email);
		UserDao userDao =new UserDao();
		if (userDao.check(user)) {
			return "register_success";
		}else {
			this.addFieldError("registration_error", getText("login.user_login_account_or_pwd_incorrect"));
			return "register_fail";
		}
		
	}

	public String getAccount() {
		return account;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}
	
	public void setAccount(String account) {
		this.account = account;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public void validate() {
		System.out.println("RegisterAction.validate...");
		if (account == null || account.trim().equals("")) {
			this.addFieldError("register_error", this.getText("login.user_login_account_not_null_info", new String[]{"login.user_name"}));
		} 
		if (password == null || password.trim().equals("") || confirmPassword == null || confirmPassword.trim().equals("")) {
			this.addFieldError("register_error", this.getText("login.user_login_pwd_is_null"));
		}
		if (password.length() < 4 || password.length() > 6) {
			this.addFieldError("register_error", this.getText("login.user_login_set_password_length"));
		}
		if (password.trim().equals(confirmPassword.trim())) {
			this.addFieldError("register_error", this.getText("login.user_register_password_unmatched"));
		}
		/*String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
		Pattern regex = Pattern.compile(check);
		Matcher matcher = regex.matcher(email);
		if (!matcher.matches()) {
			this.addFieldError("login_error", this.getText("login.user_login_email_format_incorrect"));
		}*/
		super.validate();
	}
}
