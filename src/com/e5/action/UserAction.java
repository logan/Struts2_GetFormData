package com.e5.action;

import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport{
	
	public String add() {
		System.out.println("UserAction.add...");
		return SUCCESS;
	}
	public String delete() {
		System.out.println("UserAction.delete...");
		return SUCCESS;
	}
	public String modify() {
		System.out.println("UserAction.modify...");
		return SUCCESS;
	}
}
