package com.e5.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.PreResultListener;

public class TestPreResultListener implements PreResultListener {

	@Override
	public void beforeResult(ActionInvocation invocation, String resultCode) {
		try {
			System.out.println("TestPreResultListener.beforeResult");
		} catch (Exception e) {

			e.printStackTrace();
		}
		//System.out.println("TestPreResultListener.do_preResultListener...");
	}

}
