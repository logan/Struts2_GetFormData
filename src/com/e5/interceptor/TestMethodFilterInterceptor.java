package com.e5.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

public class TestMethodFilterInterceptor extends MethodFilterInterceptor {

	@Override
	protected String doIntercept(ActionInvocation invocation) throws Exception {
		invocation.addPreResultListener(new TestPreResultListener());
		System.out.println("TestMethodFilterInterceptor.start.doIntercept...");
		String rlt =invocation.invoke();
		System.out.println("TestMethodFilterInterceptor.end.doIntercept...");
		//invocation.addPreResultListener(new TestPreResultListener());
		return rlt;
	}

}
