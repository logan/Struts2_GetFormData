package com.e5.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class TestInterceptor extends AbstractInterceptor {

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {

		System.out.println("TestInterceptor.befor.intercept...");
		String rltString = invocation.invoke();
		System.out.println("TestInterceptor.intercept.rltString: "+rltString);
		System.out.println("TestInterceptor.after.intercept...");
		return rltString;
	}

}
