package com.e5.model;

public class User {
	private String account;
	private String confirmPassword;
	private String education;
	private String email;
	private String[] favourite;
	private String gender;
	private String introduce;
	private Boolean isAccept;
	private String password;
	
	public String getAccount() {
		return account;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public String getEducation() {
		return education;
	}

	public String getEmail() {
		return email;
	}

	public String[] getFavourite() {
		return favourite;
	}

	public String getGender() {
		return gender;
	}

	public String getIntroduce() {
		return introduce;
	}

	public Boolean getIsAccept() {
		return isAccept;
	}

	public String getPassword() {
		return password;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFavourite(String[] favourite) {
		this.favourite = favourite;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public void setIsAccept(Boolean isAccept) {
		this.isAccept = isAccept;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
