<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
>
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'admin.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>

<body>
	<center>
	<img alt="" src="images/admin.ico"><br>
	<h1>管理员界面!</h1> <hr>
	<a href="<%=path %>/admin/Admin_add.action?method=添加">添加</a><br>
	<a href="<%=path %>/admin/Admin_delete.action?method=删除">删除</a><br>
	<a href="<%=path %>/admin/Admin_modify.action?method=修改">修改</a><br>
	<hr>
	<s:fielderror></s:fielderror>
	<s:form name="fileUploadForm" action="fileUpload" method="post" enctype="multipart/form-data">
		<s:file name="userImage" label="User Image" ></s:file>
		<br>
		<s:submit value="upload"></s:submit>
	</s:form>
		
	</center>
	
</body>
</html>
