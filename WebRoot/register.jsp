<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'login2.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>

<body>
<a href="lang.action?request_locale=zh_CN">中文</a>
<a href="lang.action?request_locale=en_US">English</a>
	<center>
	<img alt="" src="images/register.ico">
		<h1><s:text name="login.user_register"/></h1>
		<a href="<%=path%>/register.jsp"><s:property value="%{getText('login.user_register')}"/></a><br>
		<s:fielderror cssStyle="color:red"></s:fielderror>
<!-- 		<form action="<%=path%>/user/login.action" method="post"> -->
<!-- 			<label>{getText('login.user_name')}</label> <input type="text" name="account" value=""><br> -->
<!-- 			<label>密码</label> <input type="password" name="password"><br> -->
<!-- 			<label>邮箱</label> <input type="text" name="email"><br> -->
<!-- 			<input type="submit" key="login.user_name"> -->
<!-- 		</form> -->
		<hr>
		<s:form name="registerForm" action="user/register.action" method="post">
			<s:textfield name="user.account" label="%{getText('login.user_name')}" required="true"></s:textfield>
			<s:password name="user.password" label="%{getText('login.user_password')}" required="true"></s:password>
			<s:password name="user.confirmPassword" label="%{getText('login.user_confirmPassword')}" required="true"></s:password>
<!-- 			<s:textfield name="user.email" label="%{getText('login.user_email')}"></s:textfield> -->
			<s:submit value="%{getText('login.user_submit')}"></s:submit>
		</s:form>
<!-- 		<form action="<%=path%>/user/login.action" method="post"> -->
<!-- 			<s:textfield name="account" key="login.user_name"></s:textfield><br> -->
<!-- 			<s:password name="password" key="login.user_password"></s:password><br> -->
<!-- 			<s:textfield name="email" key="login.user_email"></s:textfield><br> -->
<!-- 			<s:token></s:token> -->
<!-- 			<input type="submit" value="<s:property value="getText('login.user_submit')"/>"/>  -->
<!-- 		</form> -->
		<br>
		<%
			out.println("admin path: " + path);
		%><br>
		<a href="<%=path%>/admin/admin.jsp"><s:property value="%{getText('login.admin_login')}"/></a><br>
</center>
</body>
</html>
