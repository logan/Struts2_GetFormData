<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'login_success.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>

<body>
	<center>
	
		<h1><s:property value="%{getText('login.user_success_info')}"/></h1>
		<img alt="" src="images/success.ico">
		<hr>
		<label>用户名是：</label>
		<s:property value="account"></s:property>
		<br> <label>密码是：</label>
		<s:property value="password" />
		<br> <label>邮箱是：</label>
		<s:property value="email" />
		<br> 
		<s:property value="login_error"/>
		<br> 
		<s:fielderror cssClass="color:red"/>
		<br>
		
		<a href="<%=path%>/user/User_add.action?method=添加">添加</a><br>
		<a href="<%=path%>/user/User_delete.action?method=删除">删除</a><br>
		<a href="<%=path%>/user/User_modify.action?method=修改">修改</a><br>

		<s:debug></s:debug>
	</center>
</body>
</html>
